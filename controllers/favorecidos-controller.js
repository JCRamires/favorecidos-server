const Favorecido = require('../models/favorecido-model')

getFavorecidos = async (req, res) => {
  await Favorecido.find({ deletado: false }, (erro, favorecidos) => {
    if (erro) {
      return res.status(400).json({ sucess: false, error: erro })
    }

    return res.status(200).json(favorecidos)
  }).catch((erro) => console.error(erro))
}

cadastrarFavorecido = (req, res) => {
  const body = req.body

  if (!body) {
    return res.status(400).json({
      success: false,
      error: 'Request invalido',
    })
  }

  const favorecido = new Favorecido(body)

  if (!favorecido) {
    return res
      .status(400)
      .json({ success: false, error: 'Erro ao criar o model Favorecido' })
  }

  favorecido
    .save()
    .then(() => {
      return res.status(201).json({ success: true, id: favorecido._id })
    })
    .catch((error) => {
      return res
        .status(400)
        .json({ error, message: 'Falha ao salvar o Favorecido' })
    })
}

deletarFavorecidos = async (req, res) => {
  const idsParaDeletar = req.body

  await Favorecido.updateMany(
    { _id: { $in: idsParaDeletar } },
    { deletado: true },
    (err, response) => {
      if (err) {
        return res.status(400).json({ success: false, error: err })
      }

      return res.status(200).json({ success: true })
    }
  ).catch((err) => console.error(err))
}

editarFavorecido = async (req, res) => {
  const { idFavorecidoEditar } = req.params
  const dados = req.body

  Favorecido.updateOne({ _id: idFavorecidoEditar }, dados, (err, response) => {
    if (err) {
      return res.status(400).json({ sucess: false, error: err })
    }

    return res.status(200).json({ success: true })
  })
}

module.exports = {
  getFavorecidos,
  cadastrarFavorecido,
  deletarFavorecidos,
  editarFavorecido,
}
