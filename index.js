const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const popularDB = require('./db/popularDB').popularDB

const db = require('./db')
const favorecidosRouter = require('./routes/favorecidos-router')

const app = express()

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use(cors())

db.on('error', console.error.bind(console, 'MongoDB connection error:'))

app.use('/api', favorecidosRouter)

popularDB()

app.listen(process.env.PORT || 5000, () =>
  console.log(`Server running on port ${process.env.PORT || 5000}`)
)
