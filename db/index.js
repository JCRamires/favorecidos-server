const mongoose = require('mongoose')

mongoose
  .connect(process.env.MONGO_URI || 'mongodb://127.0.0.1:27017/favorecidos', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .catch((e) => {
    console.error(e.message)
  })

const db = mongoose.connection

module.exports = db
