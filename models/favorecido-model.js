const mongoose = require('mongoose')

const { Schema } = mongoose

const Favorecido = new Schema({
  nome: { type: String, required: true },
  documento: { type: String, maxlength: 14, require: true },
  email: { type: String, required: true },
  banco: { type: String, enum: ['001', '104', '237', '756'], required: true },
  agencia: { type: String, maxlength: 10, required: true },
  digito_agencia: { type: String, maxlength: 1, required: true },
  tipo_conta: {
    type: String,
    enum: ['CONTA_CORRENTE', 'CONTA_POUPANCA', 'CONTA_FACIL'],
    required: true,
  },
  conta: { type: String, maxlength: 11, required: true },
  digito_conta: { type: String, maxlength: 1, required: true },
  status: { type: Number, enum: [1, 2], default: 1 },
  deletado: { type: Boolean, default: false },
})

module.exports = mongoose.model('favorecidos', Favorecido)
