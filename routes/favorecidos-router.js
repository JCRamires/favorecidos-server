const express = require('express')

const FavorecidosController = require('../controllers/favorecidos-controller')

const router = express.Router()

router.get('/favorecidos', FavorecidosController.getFavorecidos)
router.post('/favorecidos/novo', FavorecidosController.cadastrarFavorecido)
router.delete('/favorecidos/deletar', FavorecidosController.deletarFavorecidos)
router.put('/favorecidos/:idFavorecidoEditar/editar', FavorecidosController.editarFavorecido)

module.exports = router